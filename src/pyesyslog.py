#!/usr/bin/env python3

##
# __legal_b__
##
# Copyright (c) 2020-2022 Michel Gillet
# Distributed under the wxWindows Library Licence, Version 3.1.
# (See accompanying file LICENSE_3_1.txt or
# copy at http://www.wxwidgets.org/about/licence)
##
# __legal_e__
##

import os
import sys

if "PYSWIG" in os.environ:
    PYSWIG_DIR = os.path.abspath(os.environ.get("PYSWIG"))
    print("pySwig found at %s" % PYSWIG_DIR)
    sys.path.append(PYSWIG_DIR)

import pyswig

print("pyesyslog.py begins ...\n")

file_config = pyswig.FileConfig()
file_config.set_input_dir("../include/esys/log")
file_config.set_base_inc_dir("esys/log")

# path to all sources, but relative to the input directory given above
source = ["version.h",
          "level.h",
          "loggertype.h",
          "logger_if.h",
          "loggerbase.h",
          "user.h",
          "mngr.h",
          ]

file_config.set_source_files(source)

pyswig_obj = pyswig.PySwig()
pyswig_obj.add_file_config(file_config)

file_config_extra = pyswig.FileConfig()
file_config_extra.set_input_dir("../include/pyesyslog")
file_config_extra.set_src_output_dir("./pyesyslog")

# path to all sources, but relative to the input directory given above
source_extra = ["pyversion.h"
                ]

file_config_extra.set_source_files(source_extra)

pyswig_obj.add_file_config(file_config_extra)

# Configure the generation of the input files for Swig
module_name = "esyslog"
if "SWIG_TARGET_PYTHON_VERSION" in os.environ:
    target_version = os.environ["SWIG_TARGET_PYTHON_VERSION"]
    print("WARNING: SWIG_TARGET_PYTHON_VERSION = %s" % target_version)
    target_version = target_version.replace(".", "_")
    module_name += "_py" + target_version

pyswig_obj.set_module_name(module_name)
pyswig_obj.set_use_director(True)
pyswig_obj.set_src_output_dir("./pyesyslog")
pyswig_obj.set_inc_output_dir("../include/pyesyslog")
pyswig_obj.set_output_file_ext("hh")

pyswig_obj.add_typemap("std_string.i")
pyswig_obj.add_typemap("std_vector.i")
pyswig_obj.add_typemap("stdint.i")
pyswig_obj.add_typemap("std_shared_ptr.i")

pyswig_obj.add_include("stdio.h", False)
pyswig_obj.add_include("esys/log/esyslog_defs.h")

pyswig_obj.add_define("ESYSLOG_API")
pyswig_obj.add_define("PYESYSLOG_API")

pyswig_obj.generate()

pyswig_obj.set_all_warnings()
pyswig_obj.set_process_cpp()
pyswig_obj.set_language("python")

pyswig_obj.run_swig()

print("pyesyslog.py ends.")
