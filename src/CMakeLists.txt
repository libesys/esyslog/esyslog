#
# cmake-format: off
# __legal_b__
#
# Copyright (c) 2021 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
#
# __legal_e__
# cmake-format: on
#

add_subdirectory(esys/log)
