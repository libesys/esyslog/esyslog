/*!
 * \file esys/log/elapsetimer.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/esyslog_prec.h"
#include "esys/log/elapsetimer.h"

#include <sstream>

namespace esys
{

namespace log
{

ElapseTimer::ElapseTimer(const std::string &done_msg, std::shared_ptr<Logger_if> logger_if, Level log_level,
                         int debug_level)
    : m_done_msg(done_msg)
    , m_logger_if(logger_if)
    , m_log_level(log_level)
    , m_debug_level(debug_level)
{
    m_start_time = std::chrono::steady_clock::now();
}

ElapseTimer::ElapseTimer(const std::string &start_msg, const std::string &done_msg,
    std::shared_ptr<Logger_if> logger_if,
    Level log_level, int debug_level)
    : m_done_msg(done_msg)
    , m_logger_if(logger_if)
    , m_log_level(log_level)
    , m_debug_level(debug_level)
{
    m_start_time = std::chrono::steady_clock::now();
    if (m_logger_if)
        m_logger_if->log(start_msg, m_log_level, m_debug_level);
}

ElapseTimer::~ElapseTimer()
{
    std::ostringstream oss;

    auto stop_time = std::chrono::steady_clock::now();
    auto d_milli = std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - m_start_time).count();

    oss << m_done_msg << std::endl;
    oss << "    elapsed time (s): " << (d_milli / 1000) << "." << (d_milli % 1000);

    if (m_logger_if)
        m_logger_if->log(oss.str(), m_log_level, m_debug_level);
}

} // namespace log

} // namespace esys
