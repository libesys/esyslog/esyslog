/*!
 * \file esys/log/mngr.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/esyslog_prec.h"
#include "esys/log/mngr.h"
#include "esys/log/mngrimpl.h"

namespace esys
{

namespace log
{

struct Mngr::MakeSharedEnabler : public Mngr
{
    MakeSharedEnabler()
        : Mngr()
    {
    }
};

std::shared_ptr<Mngr> Mngr::s_mngr;

std::shared_ptr<Mngr> Mngr::get()
{
    if (s_mngr == nullptr) s_mngr = std::make_shared<MakeSharedEnabler>();

    return s_mngr;
}

Mngr::Mngr()
{
    m_impl = std::make_unique<MngrImpl>(this);
}

Mngr::~Mngr()
{
}

std::shared_ptr<LoggerBase> Mngr::new_default_logger(const std::string &name)
{
    if (load_if_not_loaded() < 0) return nullptr;

    auto &plugin_mngr = get_impl()->get_plugin_mngr();

    if (plugin_mngr.get_size() == 0) return nullptr;

    return plugin_mngr.get(0)->new_logger_base(name);
}

std::shared_ptr<LoggerBase> Mngr::new_logger(LoggerType type, const std::string &name)
{
    if (load_if_not_loaded() < 0) return nullptr;

    auto &plugin_mngr = get_impl()->get_plugin_mngr();
    auto plugin = plugin_mngr.get(type);

    if (plugin == nullptr) return nullptr;

    return plugin->new_logger_base(name);
}

int Mngr::load_if_not_loaded()
{
    int result = 0;

    auto &plugin_mngr = get_impl()->get_plugin_mngr();

    if (!plugin_mngr.get_is_loaded()) result = plugin_mngr.load();

    return result;
}

void Mngr::set_search_folder(const std::string &search_folder)
{
    get_impl()->get_plugin_mngr().set_search_folder(search_folder);
}

const std::string &Mngr::get_search_folder() const
{
    return get_impl()->get_plugin_mngr().get_search_folder();
}

int Mngr::find_plugin_folders(std::vector<std::string> &plugin_folders)
{
    return get_impl()->get_plugin_mngr().find_plugin_folders(plugin_folders);
}

MngrImpl *Mngr::get_impl()
{
    return m_impl.get();
}

const MngrImpl *Mngr::get_impl() const
{
    return m_impl.get();
}

} // namespace log

} // namespace esys
