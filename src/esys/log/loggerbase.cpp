/*!
 * \file esys/lob/loggerbase.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/esyslog_prec.h"
#include "esys/log/loggerbase.h"

namespace esys
{

namespace log
{

LoggerBase::LoggerBase(const std::string &name)
    : Logger_if()
    , m_name(name)
{
}

LoggerBase::~LoggerBase()
{
}

void LoggerBase::set_type(LoggerType type)
{
    m_type = type;
}

LoggerType LoggerBase::get_type() const
{
    return m_type;
 }

void LoggerBase::set_name(const std::string &name)
{
    m_name = name;
}

const std::string &LoggerBase::get_name()
{
    return m_name;
}

/*int LoggerBase::add_console(Level log_level)
{
    return m_impl->add_console(log_level);
}

int LoggerBase::add_console(const std::string &pattern, log::Level log_level)
{
    return m_impl->add_console(pattern, log_level);
}

int LoggerBase::add_basic_file(const std::string &path, bool truncate, log::Level log_level)
{
    return m_impl->add_basic_file(path, truncate, log_level);
}

int LoggerBase::add_rotating_file(const std::string &path, std::size_t max_file_size_mbytes, std::size_t max_file_count,
                                  log::Level log_level)
{
    return m_impl->add_rotating_file(path, max_file_size_mbytes, max_file_count, log_level);
} */

void LoggerBase::debug(int level, const std::string &msg)
{
    if (get_log_level() < log::Level::DEBUG) return;
    if (level > get_debug_level()) return;

    impl_debug(msg);
}

/*void LoggerBase::info(const std::string &msg)
{
    m_impl->info(msg);
}

void LoggerBase::warn(const std::string &msg)
{
    m_impl->warn(msg);
}

void LoggerBase::error(const std::string &msg)
{
    m_impl->error(msg);
}

void LoggerBase::critical(const std::string &msg)
{
    m_impl->critical(msg);
} */

void LoggerBase::set_log_level(log::Level log_level)
{
    m_log_level = log_level;
}

log::Level LoggerBase::get_log_level() const
{
    return m_log_level;
}

void LoggerBase::set_flush_log_level(log::Level flush_log_level)
{
    m_flush_log_level = flush_log_level;
}

log::Level LoggerBase::get_flush_log_level() const
{
    return m_flush_log_level;
}

void LoggerBase::set_flush_every(int flush_every_secs)
{
    m_flush_every_secs = m_flush_every_secs;
}

int LoggerBase::get_flush_every() const
{
    return m_flush_every_secs;
}

void LoggerBase::set_debug_level(int debug_level)
{
    m_debug_level = debug_level;
}

int LoggerBase::get_debug_level() const
{
    return m_debug_level;
}

} // namespace log

} // namespace esys
