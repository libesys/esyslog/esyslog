/*!
 * \file esys/log/pluginmngr.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2022 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/esyslog_prec.h"
#include "esys/log/pluginmngr.h"
#include "esys/log/plugin.h"

#ifdef ESYSLOG_SPDLOG_STATIC
#include "esys/log/spdlog/plugin.h"
#endif

#include <cassert>

namespace esys::log
{

PluginMngr::PluginMngr()
    : BaseType("esyslog")
{
    set_entry_fct_name("get_esyslog_plugin");

    // Needed if Logger is used from Python code, as the location
    // of the Python executable is not useful
    add_env_var_search_folder("PYESYSLOG_INST_DIR");
    add_env_var_search_folder("ESYSLOG_INST_DIR");
    add_env_var_search_folder("PYESYSREPO_INST_DIR");
    add_env_var_search_folder("ESYSREPO_INST_DIR");
    add_env_var_search_folder("PYESYSSDK_INST_DIR");
    add_env_var_search_folder("ESYSSDK_INST_DIR");

#ifdef ESYSLOG_SPDLOG_STATIC
    auto spdlog_plugin = std::make_shared<spdlog::Plugin>();
    add_static_plugin(spdlog_plugin);
#endif
}

PluginMngr::~PluginMngr() = default;

base::PluginBase *PluginMngr::get_plugin_from_entry_fct(void *entry_fct)
{
    PluginEntryFunction the_entry_fct = (PluginEntryFunction)entry_fct;
    Plugin *plugin;

    assert(entry_fct != nullptr);

    plugin = (*the_entry_fct)();

    return plugin;
}

int PluginMngr::plugin_loaded(Plugin *plugin)
{
    if (plugin->get_logger_type() != LoggerType::NOT_SET) m_map_logger_type_plugin[plugin->get_logger_type()] = plugin;

    return 0;
}

Plugin *PluginMngr::get(LoggerType logger_type)
{
    auto it = m_map_logger_type_plugin.find(logger_type);

    if (it != m_map_logger_type_plugin.end()) return it->second;

    return nullptr;
}

std::map<LoggerType, Plugin *> PluginMngr::get_map_logger_type_plugin()
{
    return m_map_logger_type_plugin;
}

const std::map<LoggerType, Plugin *> PluginMngr::get_map_logger_type_plugin() const
{
    return m_map_logger_type_plugin;
}

} // namespace esys::log
