/*!
 * \file esys/log/logger_if.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/esyslog_prec.h"
#include "esys/log/logger_if.h"

namespace esys
{

namespace log
{

Logger_if::Logger_if() = default;

Logger_if::~Logger_if() = default;

} // namespace log

} // namespace esys
