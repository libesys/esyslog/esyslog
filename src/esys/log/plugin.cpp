/*!
 * \file esys/log/plugin.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/esyslog_prec.h"
#include "esys/log/plugin.h"

namespace esys
{

namespace log
{

Plugin::Plugin()
    : PluginBase()
{
}

Plugin::~Plugin()
{
}

void Plugin::set_logger_type(LoggerType logger_type)
{
    m_logger_type = logger_type;
}

LoggerType Plugin::get_logger_type() const
{
    return m_logger_type;
}

} // namespace log

} // namespace esys
