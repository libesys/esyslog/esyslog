/*!
 * \file esys/log/trace.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/esyslog_prec.h"
#include "esys/log/trace.h"

namespace esys::log
{

Trace::Trace() = default;

Trace::~Trace() = default;

void Trace::lock()
{
}

void Trace::unlock()
{
}

void Trace::write(const std::string &trace)
{
    if (m_logger == nullptr) return;

    //m_logger->debug(get_debug_level(), trace);
    m_logger->trace(trace);
}

void Trace::set_logger(std::shared_ptr<LoggerBase> logger)
{
    m_logger = logger;
}
std::shared_ptr<LoggerBase> Trace::get_logger() const
{
    return m_logger;
}

void Trace::set_debug_level(int debug_level)
{
    m_debug_level = debug_level;
}

int Trace::get_debug_level() const
{
    return m_debug_level;
}

} // namespace esys::log
