/*!
 * \file esys/log/mngrimpl.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/esyslog_prec.h"
#include "esys/log/mngrimpl.h"

namespace esys::log
{

MngrImpl::MngrImpl(Mngr *self)
    : m_self(self)
{
}

MngrImpl::~MngrImpl()
{
    m_plugin_mngr.release();
}

PluginMngr &MngrImpl::get_plugin_mngr()
{
    return m_plugin_mngr;
}

const PluginMngr &MngrImpl::get_plugin_mngr() const
{
    return m_plugin_mngr;
}

Mngr *MngrImpl::self()
{
    return m_self;
}

const Mngr *MngrImpl::self() const
{
    return m_self;
}

} // namespace esys::log
