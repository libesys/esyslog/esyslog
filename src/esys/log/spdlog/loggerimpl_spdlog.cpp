/*!
 * \file esys/log/spdlog/loggerimpl.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/spdlog/esyslog_spdlog_prec.h"
#include "esys/log/spdlog/loggerimpl.h"

#include <iostream>

namespace esys::log::spdlog
{

LoggerImpl::LoggerImpl(Logger *self)
    : m_self(self)
{
}

LoggerImpl::~LoggerImpl()
{
    if (get_logger()) get_logger()->flush();
    if (!m_self->get_name().empty()) ::spdlog::drop(m_self->get_name());
}

int LoggerImpl::add_console(log::Level log_level)
{
    if (m_console_sink != nullptr) return -1;

    m_console_sink = std::make_shared<::spdlog::sinks::stdout_color_sink_mt>();
    m_console_sink->set_level(to_spdlog_level(log_level));
    m_console_sink->set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%n] [%^%l%$] %v");
    get_logger()->sinks().push_back(m_console_sink);
    return 0;
}

int LoggerImpl::add_console(const std::string &pattern, log::Level log_level)
{
    if (m_console_sink != nullptr) return -1;

    m_console_sink = std::make_shared<::spdlog::sinks::stdout_color_sink_mt>();
    m_console_sink->set_level(to_spdlog_level(log_level));
    m_console_sink->set_pattern(pattern);
    get_logger()->sinks().push_back(m_console_sink);
    return 0;
}

void LoggerImpl::lock_console()
{
    if (m_console_sink == nullptr) return;

    m_console_sink->lock();
}

void LoggerImpl::unlock_console()
{
    if (m_console_sink == nullptr) return;

    m_console_sink->unlock();
}

int LoggerImpl::add_basic_file(const std::string &path, bool truncate, log::Level log_level)
{
    if (m_basic_file_sink != nullptr) return -1;
    if (m_rotating_file_sink != nullptr) return -1;

    m_basic_file_sink = std::make_shared<::spdlog::sinks::basic_file_sink_mt>(path, truncate);
    m_basic_file_sink->set_level(to_spdlog_level(log_level));
    get_logger()->sinks().push_back(m_basic_file_sink);
    return 0;
}

int LoggerImpl::add_rotating_file(const std::string &path, std::size_t max_file_size_mbytes, std::size_t max_file_count,
                                  log::Level log_level)
{
    if (m_basic_file_sink != nullptr) return -1;
    if (m_rotating_file_sink != nullptr) return -1;

    std::size_t max_file_size = 1024 * 1024 * max_file_size_mbytes;

    m_rotating_file_sink =
        std::make_shared<::spdlog::sinks::rotating_file_sink_mt>(path, max_file_size, max_file_count);
    m_rotating_file_sink->set_level(to_spdlog_level(log_level));
    get_logger()->sinks().push_back(m_rotating_file_sink);
    return 0;
}

void LoggerImpl::trace(const std::string &msg)
{
    get_logger()->trace(msg);
}

void LoggerImpl::debug(const std::string &msg)
{
    get_logger()->debug(msg);
}

void LoggerImpl::info(const std::string &msg)
{
    get_logger()->info(msg);
}

void LoggerImpl::warn(const std::string &msg)
{
    get_logger()->warn(msg);
}

void LoggerImpl::error(const std::string &msg)
{
    get_logger()->error(msg);
}

void LoggerImpl::critical(const std::string &msg)
{
    get_logger()->critical(msg);
}

void LoggerImpl::log(Level level, const std::string &msg)
{
    get_logger()->log(to_spdlog_level(level), msg);
}

void LoggerImpl::log(const std::string &msg, log::Level level, int debug_level)
{
    if (level != log::Level::DEBUG)
        log(level, msg);
    else
        self()->debug(debug_level, msg);
}

void LoggerImpl::set_log_level(log::Level log_level)
{
    get_logger()->set_level(to_spdlog_level(log_level));
}

void LoggerImpl::set_flush_log_level(log::Level flush_log_level)
{
    get_logger()->flush_on(to_spdlog_level(flush_log_level));
}

::spdlog::level::level_enum LoggerImpl::to_spdlog_level(log::Level log_level)
{
    switch (log_level)
    {
        case log::Level::CRITICAL: return ::spdlog::level::critical;
        case log::Level::DEBUG: return ::spdlog::level::debug;
        case log::Level::ERROR: return ::spdlog::level::err;
        case log::Level::INFO: return ::spdlog::level::info;
        case log::Level::TRACE: return ::spdlog::level::trace;
        case log::Level::WARN: return ::spdlog::level::warn;
        default: return ::spdlog::level::debug;
    }
}

std::shared_ptr<::spdlog::logger> LoggerImpl::get_logger()
{
    if (m_logger != nullptr) return m_logger;

    m_logger = std::make_shared<::spdlog::logger>(self()->get_name());
    m_logger->set_level(::spdlog::level::trace);

    if (self()->get_name().empty())
        std::cerr << "ERROR: a Logger was not given a name" << std::endl;
    else
        ::spdlog::register_logger(m_logger);

    if (self()->get_flush_every() != -1) ::spdlog::flush_every(std::chrono::seconds(self()->get_flush_every()));
    return m_logger;
}

Logger *LoggerImpl::self()
{
    return m_self;
}

} // namespace esys::log::spdlog
