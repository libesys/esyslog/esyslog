/*!
 * \file esys/log/spdlog/plugin_spdlog.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/spdlog/esyslog_spdlog_prec.h"
#include "esys/log/spdlog/plugin.h"
#include "esys/log/spdlog/version.h"
#include "esys/log/spdlog/logger.h"

DEFINE_ESYSLOG_PLUGIN(ESYSLOG_SPDLOG_API, esys::log::spdlog::Plugin);

namespace esys::log::spdlog
{

Plugin::Plugin()
    : BaseType()
{
    set_name("spdlog");
    set_short_name("spdlog");
    set_version(ESYSLOG_SPDLOG_VERSION_NUM_DOT_STRING);

    set_logger_type(LoggerType::SPDLOG);
}

Plugin::~Plugin()
{
}

std::shared_ptr<LoggerBase> Plugin::new_logger_base(const std::string &name)
{
    return std::make_shared<Logger>(name);
}

} // namespace esys::log::spdlog
