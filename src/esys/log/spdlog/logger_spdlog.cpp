/*!
 * \file esys/log/spdlog/logger_spdlog.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/spdlog/esyslog_spdlog_prec.h"
#include "esys/log/spdlog/logger.h"
#include "esys/log/spdlog/loggerimpl.h"

namespace esys::log::spdlog
{

Logger::Logger(const std::string &name)
    : LoggerBase(name)
{
    m_impl = std::make_unique<LoggerImpl>(this);

    set_type(LoggerType::SPDLOG);
}

Logger::~Logger()
{
}

int Logger::add_console(Level log_level)
{
    return get_impl()->add_console(log_level);
}

int Logger::add_console(const std::string &pattern, log::Level log_level)
{
    return get_impl()->add_console(pattern, log_level);
}

void Logger::lock_console()
{
    get_impl()->lock_console();
}

void Logger::unlock_console()
{
    get_impl()->unlock_console();
}

int Logger::add_basic_file(const std::string &path, bool truncate, log::Level log_level)
{
    return get_impl()->add_basic_file(path, truncate, log_level);
}

int Logger::add_rotating_file(const std::string &path, std::size_t max_file_size_mbytes, std::size_t max_file_count,
                              log::Level log_level)
{
    return get_impl()->add_rotating_file(path, max_file_size_mbytes, max_file_count, log_level);
}

void Logger::trace(const std::string &msg)
{
    get_impl()->trace(msg);
}

void Logger::impl_debug(const std::string &msg)
{
    get_impl()->debug(msg);
}

void Logger::info(const std::string &msg)
{
    get_impl()->info(msg);
}

void Logger::warn(const std::string &msg)
{
    get_impl()->warn(msg);
}

void Logger::error(const std::string &msg)
{
    get_impl()->error(msg);
}

void Logger::critical(const std::string &msg)
{
    get_impl()->critical(msg);
}

void Logger::log(Level level, const std::string &msg)
{
    get_impl()->log(level, msg);
}

void Logger::log(const std::string &msg, log::Level level, int debug_level)
{
    get_impl()->log(msg, level, debug_level);
}

void Logger::set_log_level(log::Level log_level)
{
    m_log_level = log_level;
}

void Logger::set_flush_log_level(log::Level flush_log_level)
{
    m_flush_log_level = flush_log_level;
}

void Logger::set_flush_every(int flush_every_secs)
{
    m_flush_every_secs = m_flush_every_secs;
}

void Logger::set_debug_level(int debug_level)
{
    m_debug_level = debug_level;
}

LoggerImpl *Logger::get_impl()
{
    return m_impl.get();
}

} // namespace esys::log::spdlog
