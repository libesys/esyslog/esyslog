/*!
 * \file esys/log/spdlog/loggerimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/log/spdlog/esyslog_spdlog_defs.h"
#include "esys/log/spdlog/logger.h"

#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/rotating_file_sink.h>

namespace esys::log::spdlog
{

class ESYSLOG_SPDLOG_API LoggerImpl
{
public:
    LoggerImpl(Logger *self);
    ~LoggerImpl();

    int add_console(Level log_level = Level::INFO);
    int add_console(const std::string &pattern, Level log_level = Level::INFO);
    void lock_console();
    void unlock_console();

    int add_basic_file(const std::string &path, bool truncate = false, Level log_level = Level::DEBUG);
    int add_rotating_file(const std::string &path, std::size_t max_file_size_mbytes, std::size_t max_file_count,
                          Level log_level = Level::DEBUG);

    void trace(const std::string &msg);
    void debug(const std::string &msg);
    void info(const std::string &msg);
    void warn(const std::string &msg);
    void error(const std::string &msg);
    void critical(const std::string &msg);
    void log(Level level, const std::string &msg);
    void log(const std::string &msg, log::Level level, int debug_level);

    void set_log_level(Level log_level);
    void set_flush_log_level(Level flush_log_level);

    ::spdlog::level::level_enum to_spdlog_level(Level log_level);

    std::shared_ptr<::spdlog::logger> get_logger();

    Logger *self();

private:
    Logger *m_self = nullptr;
    std::shared_ptr<::spdlog::logger> m_logger;
    std::shared_ptr<::spdlog::sinks::stdout_color_sink_mt> m_console_sink;
    std::shared_ptr<::spdlog::sinks::basic_file_sink_mt> m_basic_file_sink;
    std::shared_ptr<::spdlog::sinks::rotating_file_sink_mt> m_rotating_file_sink;
};

} // namespace esys::log::spdlog
