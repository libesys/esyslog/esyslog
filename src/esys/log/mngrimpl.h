/*!
 * \file esys/log/mngrimpl.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/log/esyslog_defs.h"
#include "esys/log/pluginmngr.h"
#include "esys/log/mngr.h"

namespace esys
{

namespace log
{

/*! \class MngrImpl esys/log/mngrimpl.h "esys/log/mngrimpl.h"
 * \brief Mngr
 */
class ESYSLOG_API MngrImpl
{
public:
    //! Constructor
    MngrImpl(Mngr *self);

    //! Destructor
    ~MngrImpl();

    PluginMngr &get_plugin_mngr();
    const PluginMngr &get_plugin_mngr() const;

    Mngr *self();
    const Mngr *self() const;

protected:
    Mngr *m_self = nullptr;

    PluginMngr m_plugin_mngr;
};

} // namespace log

} // namespace esys
