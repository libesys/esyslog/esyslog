/*!
 * \file eys/log/user.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/esyslog_prec.h"
#include "esys/log/user.h"

#include <sstream>

namespace esys
{

namespace log
{

User::User() = default;

User::~User() = default;

void User::trace(const std::string &msg)
{
    if (get_logger_if() == nullptr) return;

    get_logger_if()->trace(msg);
}

void User::debug(int level, const std::string &msg)
{
    if (get_logger_if() == nullptr) return;

    get_logger_if()->debug(level, msg);
}

void User::info(const std::string &msg)
{
    if (get_logger_if() == nullptr) return;

    get_logger_if()->info(msg);
}

void User::warn(const std::string &msg)
{
    if (get_logger_if() == nullptr) return;

    get_logger_if()->warn(msg);
}

void User::error(const std::string &msg)
{
    if (get_logger_if() == nullptr) return;

    get_logger_if()->error(msg);
}

void User::critical(const std::string &msg)
{
    if (get_logger_if() == nullptr) return;

    get_logger_if()->critical(msg);
}

void User::log(Level level, const std::string &msg)
{
    if (get_logger_if() == nullptr) return;

    get_logger_if()->log(level, msg);
}

void User::log(const std::string &msg, log::Level level, int debug_level)
{
    if (get_logger_if() == nullptr) return;

    get_logger_if()->log(msg, level, debug_level);
}

void User::lock_console()
{
    if (get_logger_if() == nullptr) return;

    get_logger_if()->lock_console();
}

void User::unlock_console()
{
    if (get_logger_if() == nullptr) return;

    get_logger_if()->unlock_console();
}

void User::error(const std::string &msg, int result)
{
    std::ostringstream oss;

    oss << msg << result;

    error(oss.str());
}

void User::set_logger_if(std::shared_ptr<Logger_if> logger_if)
{
    m_logger_if = logger_if;
}

std::shared_ptr<Logger_if> User::get_logger_if()
{
    return m_logger_if;
}

} // namespace log

} // namespace esys
