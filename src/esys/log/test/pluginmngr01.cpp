/*!
 * \file esys/log/test/pluginmngr01.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/test/esyslog_t_prec.h"

#include <esys/log/pluginmngr.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys::log::test
{

/*! \class PluginMngr01 esys/log/test/pluginmngr01.cpp "esys/log/test/pluginmngr01.cpp"
 *
 *  \brief
 *
 */
ESYSTEST_AUTO_TEST_CASE(PluginMngr01)
{
    boost::filesystem::path path;

    path = TestCaseCtrl::get().GetTempFilesFolder();
    path /= ("pluginmngr01");

    bool remove_all = true;

    try
    {
        if (boost::filesystem::exists(path)) boost::filesystem::remove_all(path);
    }
    catch (const boost::filesystem::filesystem_error &e)
    {
        remove_all = false;
        std::cerr << e.what() << std::endl;
    }

    ESYSTEST_REQUIRE_EQUAL(remove_all, true);

    bool folder_created = boost::filesystem::create_directories(path);
    ESYSTEST_REQUIRE_EQUAL(folder_created, true);

    PluginMngr mngr;

    ESYSTEST_REQUIRE_EQUAL(mngr.get_is_loaded(), false);

    int result = mngr.load();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    auto &plugins = mngr.get_plugins();
    ESYSTEST_REQUIRE_EQUAL(plugins.size(), 1);

    auto plugin = plugins[0];

    ESYSTEST_REQUIRE_EQUAL(plugin->get_logger_type(), LoggerType::SPDLOG);

    result = mngr.release();
    ESYSTEST_REQUIRE_EQUAL(result, 0);
}

} // namespace esys::log::test
