/*!
 * \file esys/log/test/spdloglogger01.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "esys/log/test/esyslog_t_prec.h"

#include <esys/log/mngr.h>

#include <boost/filesystem.hpp>

#include <iostream>

namespace esys
{

namespace log
{

namespace test
{

/*! \class SpdLogLogger01 esys/log/test/spdloglogger01.cpp "esys/log/test/spdloglogger01.cpp"
 *
 *  \brief
 *
 */

ESYSTEST_AUTO_TEST_CASE(SpdLogLogger01)
{
    boost::filesystem::path path;

    path = TestCaseCtrl::get().GetTempFilesFolder();
    path /= ("spdloglogger01");

    bool remove_all = true;

    try
    {
        if (boost::filesystem::exists(path)) boost::filesystem::remove_all(path);
    }
    catch (const boost::filesystem::filesystem_error &e)
    {
        remove_all = false;
        std::cerr << e.what() << std::endl;
    }

    ESYSTEST_REQUIRE_EQUAL(remove_all, true);

    bool folder_created = boost::filesystem::create_directories(path);
    ESYSTEST_REQUIRE_EQUAL(folder_created, true);

    auto mngr = Mngr::get();

    auto logger = mngr->new_default_logger("spdloglogger01");
    ESYSTEST_REQUIRE_NE(logger, nullptr);

    int result = logger->add_console();
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    path /= "log.txt";

    result = logger->add_basic_file(path.string());
    ESYSTEST_REQUIRE_EQUAL(result, 0);

    logger->info("Info from SpdLogLogger01");
    logger->debug(0, "Debug log from SpdLogLogger01");
    logger->warn("Warning log from SpdLogLogger01");
    logger->error("Error log from SpdLogLogger01");
}

} // namespace test

} // namespace log

} // namespace esys
