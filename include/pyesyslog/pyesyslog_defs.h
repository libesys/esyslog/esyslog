/*!
 * \file pyesyslog/pyesyslog_defs.h
 * \brief Definitions needed for pyesyslog
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef PYESYSLOG_EXPORTS
#define PYESYSLOG_API __declspec(dllexport)
#elif PYESYSLOG_USE
#define PYESYSLOG_API __declspec(dllimport)
#else
#define PYESYSLOG_API
#endif
