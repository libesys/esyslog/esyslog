/*!
 * \file pyesyslog/pyversion.h
 * \brief Version info for pyesyslog
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define PYESYSLOG_MAJOR_VERSION 0
#define PYESYSLOG_MINOR_VERSION 1
#define PYESYSLOG_RELEASE_NUMBER 0
#define PYESYSLOG_VERSION_STRING "pyesyslog 0.1.0"

// Must be updated manually as well each time the version above changes
#define PYESYSLOG_VERSION_NUM_DOT_STRING "0.1.0"
#define PYESYSLOG_VERSION_NUM_STRING "000100"

// nothing should be updated below this line when updating the version

#define PYESYSLOG_VERSION_NUMBER \
    (PYESYSLOG_MAJOR_VERSION * 1000) + (PYESYSLOG_MINOR_VERSION * 100) + PYESYSLOG_RELEASE_NUMBER
#define PYESYSLOG_BETA_NUMBER 1
#define PYESYSLOG_VERSION_FLOAT                                                                     \
    PYESYSLOG_MAJOR_VERSION + (PYESYSLOG_MINOR_VERSION / 10.0) + (PYESYSLOG_RELEASE_NUMBER / 100.0) \
        + (PYESYSLOG_BETA_NUMBER / 10000.0)

// check if the current version is at least major.minor.release
#define PYESYSLOG_CHECK_VERSION(major, minor, release)                                                              \
    (PYESYSLOG_MAJOR_VERSION > (major) || (PYESYSLOG_MAJOR_VERSION == (major) && PYESYSLOG_MINOR_VERSION > (minor)) \
     || (PYESYSLOG_MAJOR_VERSION == (major) && PYESYSLOG_MINOR_VERSION == (minor)                                   \
         && PYESYSLOG_RELEASE_NUMBER >= (release)))
