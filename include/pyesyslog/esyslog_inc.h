// PySwig 0.1.0

#pragma once

#include "esys/log/version.h"
#include "esys/log/level.h"
#include "esys/log/loggertype.h"
#include "esys/log/logger_if.h"
#include "esys/log/loggerbase.h"
#include "esys/log/user.h"
#include "esys/log/mngr.h"
#include "pyversion.h"


