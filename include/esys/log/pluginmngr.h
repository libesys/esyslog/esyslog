/*!
 * \file esys/log/pluginmngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2022 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/log/esyslog_defs.h"
#include "esys/log/plugin.h"

#include <esys/base/pluginmngrcore_t.h>

#include <vector>
#include <map>

//<swig_inc/>

namespace esys::log
{

/*! \class PluginMngr esys/log/pluginmngr.h "esys/log/pluginmngr.h"
 * \brief Plugin manager class for esyslog
 *
 */
class ESYSLOG_API PluginMngr : public esys::base::PluginMngrCore_t<Plugin>
{
public:
    using BaseType = esys::base::PluginMngrCore_t<Plugin>;

    using BaseType::get;

    //! Constructor
    PluginMngr();

    //! Destructor
    ~PluginMngr() override;

    base::PluginBase *get_plugin_from_entry_fct(void *entry_fct) override;

    std::map<LoggerType, Plugin *> get_map_logger_type_plugin();
    const std::map<LoggerType, Plugin *> get_map_logger_type_plugin() const;

    Plugin *get(LoggerType logger_type);

protected:
    int plugin_loaded(Plugin *plugin) override;

private:
    std::map<LoggerType, Plugin *> m_map_logger_type_plugin;
};

} // namespace esys::log
