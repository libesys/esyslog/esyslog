/*!
 * \file esys/log/user.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/log/esyslog_defs.h"
#include "esys/log/logger_if.h"

#include <memory>

//<swig_inc/>

//<swig>%shared_ptr(esys::log::User);</swig>

namespace esys
{

namespace log
{

/*! \class User esys/log/user.h "esys/log/user.h"
 * \brief User of a logger
 */
class ESYSLOG_API User : public Logger_if
{
public:
    //! Constructor
    User();

    //! Destructor
    virtual ~User();

    void trace(const std::string &msg) override;
    void debug(int level, const std::string &msg) override;
    void info(const std::string &msg) override;
    void warn(const std::string &msg) override;
    void error(const std::string &msg) override;
    void critical(const std::string &msg) override;
    void log(Level level, const std::string &msg) override;
    void log(const std::string &msg, log::Level level, int debug_level = 0) override;
    void lock_console() override;
    void unlock_console() override;

    virtual void error(const std::string &msg, int result);

    //<swig_out>
    template<typename F, typename... ARGS>
    int log_wrap(log::Level level, int debug_level, const std::string &msg, F &&fct, ARGS &&... args)
    {
        log(msg, level, debug_level);

        int result = std::forward<F>(fct)(std::forward<ARGS>(args)...);
        if (result < 0) error("Failed with error ", result);
        return result;
    }

    template<typename F, typename... ARGS>
    auto log_wrap(const std::string &msg, F fct, ARGS... args)
    {
        return [msg, fct, args..., this](log::Level level, int debug_level = 0) {
            log(msg, level, debug_level);

            int result = fct(args...);
            if (result < 0) error("Failed with error ", result);
            return result;
        };
    }

    template<typename F>
    auto log_wrap(F fct, log::Level level, int debug_level = 0)
    {
        return [fct, level, debug_level, this](const std::string &msg, auto &&... args) {
            log(msg, level, debug_level);

            int result = fct(args...);
            if (result < 0) error("Failed with error ", result);
            return result;
        };
    }

    template<typename OBJ, typename F>
    auto log_wrap(std::shared_ptr<OBJ> obj, F fct, log::Level level, int debug_level = 0)
    {
        return [obj, fct, level, debug_level, this](const std::string &msg, auto &&... args) {
            log(msg, level, debug_level);

            int result = (obj.get()->*fct)(args...);
            if (result < 0) error("Failed with error ", result);
            return result;
        };
    }

    template<typename OBJ, typename F>
    auto log_wrap(OBJ *obj, F fct, log::Level level, int debug_level = 0)
    {
        return [obj, fct, level, debug_level, this](const std::string &msg, auto &&... args) {
            log(msg, level, debug_level);

            int result = (obj->*fct)(args...);
            if (result < 0) error("Failed with error ", result);
            return result;
        };
    }

    //</swig_out>

    //! Set the Logger interface
    /*!
     * \param[in] logger_if the Logger interface
     */
    virtual void set_logger_if(std::shared_ptr<Logger_if> logger_if);

    //! Get the Logger interface
    /*!
     * \return the Logger interface
     */
    std::shared_ptr<Logger_if> get_logger_if();

private:
    //!< \cond DOXY_IMPL
    std::shared_ptr<Logger_if> m_logger_if = nullptr; //!< The Logger interface
    //!< \endcond
};

} // namespace log

} // namespace esys
