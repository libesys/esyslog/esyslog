/*!
 * \file esys/log/plugin.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2022 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/log/esyslog_defs.h"
#include "esys/log/loggerbase.h"
#include "esys/log/plugin.h"

#include <esys/base/pluginbase.h>

#include <memory>

#define DECLARE_ESYSLOG_PLUGIN(exp) DECLARE_ESYSBASE_PLUGIN(exp, esyslog, esys::log::Plugin)

#define DEFINE_ESYSLOG_PLUGIN(exp, class_) DEFINE_ESYSBASE_PLUGIN(exp, esyslog, esys::log::Plugin, class_)

//<swig_inc/>

namespace esys::log
{

/*! \class Plugin esys/log/plugin.h "esys/log/plugin.h"
 * \brief Plugin class
 *
 */
class ESYSLOG_API Plugin : public base::PluginBase
{
public:
    //! Constructor
    Plugin();

    //! Destructor
    virtual ~Plugin();

    virtual std::shared_ptr<LoggerBase> new_logger_base(const std::string &name = "") = 0;

    void set_logger_type(LoggerType logger_type);
    LoggerType get_logger_type() const;

protected:
    LoggerType m_logger_type = LoggerType::NOT_SET;
};

typedef Plugin *(*PluginEntryFunction)();

} // namespace esys::log
