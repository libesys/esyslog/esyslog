/*!
 * \file esys/log/test/version.h
 * \brief Version info for esyslog_t
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define ESYSLOG_T_MAJOR_VERSION 0
#define ESYSLOG_T_MINOR_VERSION 1
#define ESYSLOG_T_RELEASE_NUMBER 0
#define ESYSLOG_T_VERSION_STRING "esyslog_t 0.1.0"

// Must be updated manually as well each time the version above changes
#define ESYSLOG_T_VERSION_NUM_DOT_STRING "0.1.0"
#define ESYSLOG_T_VERSION_NUM_STRING "0100"

// nothing should be updated below this line when updating the version

#define ESYSLOG_T_VERSION_NUMBER \
    (ESYSLOG_T_MAJOR_VERSION * 1000) + (ESYSLOG_T_MINOR_VERSION * 100) + ESYSLOG_T_RELEASE_NUMBER
#define ESYSLOG_T_BETA_NUMBER 1
#define ESYSLOG_T_VERSION_FLOAT                                                                     \
    ESYSLOG_T_MAJOR_VERSION + (ESYSLOG_T_MINOR_VERSION / 10.0) + (ESYSLOG_T_RELEASE_NUMBER / 100.0) \
        + (ESYSLOG_T_BETA_NUMBER / 10000.0)

// check if the current version is at least major.minor.release
#define ESYSLOG_T_CHECK_VERSION(major, minor, release)                                                              \
    (ESYSLOG_T_MAJOR_VERSION > (major) || (ESYSLOG_T_MAJOR_VERSION == (major) && ESYSLOG_T_MINOR_VERSION > (minor)) \
     || (ESYSLOG_T_MAJOR_VERSION == (major) && ESYSLOG_T_MINOR_VERSION == (minor)                                   \
         && ESYSLOG_T_RELEASE_NUMBER >= (release)))
