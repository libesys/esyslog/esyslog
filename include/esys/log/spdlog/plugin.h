/*!
 * \file esys_sysc/plugin.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/log/spdlog/esyslog_spdlog_defs.h"

#include <esys/log/plugin.h>

#include <esys/base/plugin_t.h>

namespace esys::log::spdlog
{

class ESYSLOG_SPDLOG_API Plugin : public esys::base::Plugin_t<Plugin, log::Plugin>
{
public:
    typedef esys::base::Plugin_t<Plugin, log::Plugin> BaseType;

    Plugin();
    virtual ~Plugin();

    std::shared_ptr<LoggerBase> new_logger_base(const std::string &name = "") override;

protected:
};

} // namespace esys::log::spdlog

DECLARE_ESYSLOG_PLUGIN(ESYSLOG_SPDLOG_API);
