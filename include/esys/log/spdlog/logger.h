/*!
 * \file esys/log/spdlog/logger.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/log/spdlog/esyslog_spdlog_defs.h"

#include <esys/log/loggerbase.h>

#include <memory>

namespace esys::log::spdlog
{

class ESYSLOG_SPDLOG_API LoggerImpl;

/*! \class LoggerBase esys/log/loggerbase.h "esys/log/loggerbase.h"
 * \brief Class to log to console and a log file
 */
class ESYSLOG_SPDLOG_API Logger : public LoggerBase
{
public:
    //! Constructor
    Logger(const std::string &name = "");

    //! Destructor
    virtual ~Logger();

    int add_console(Level log_level = Level::INFO) override;
    int add_console(const std::string &pattern, Level log_level = Level::INFO) override;
    void lock_console() override;
    void unlock_console() override;
    int add_basic_file(const std::string &path, bool truncate = false, Level log_level = Level::DEBUG) override;

    int add_rotating_file(const std::string &path, std::size_t max_file_size_mbytes, std::size_t max_file_count,
                          Level log_level = Level::DEBUG) override;

    void trace(const std::string &msg) override;
    void info(const std::string &msg) override;
    void warn(const std::string &msg) override;
    void error(const std::string &msg) override;
    void critical(const std::string &msg) override;
    void log(Level level, const std::string &msg) override;
    void log(const std::string &msg, log::Level level, int debug_level = 0) override;

    void set_log_level(Level log_level) override;
    void set_flush_log_level(Level log_level) override;
    void set_flush_every(int flush_every_secs) override;
    void set_debug_level(int debug_level) override;

    LoggerImpl *get_impl();

private:
    //!< \cond DOXY_IMPL
    void impl_debug(const std::string &msg) override;

    std::unique_ptr<LoggerImpl> m_impl;
    //!< \endcond
};

} // namespace esys::log::spdlog
