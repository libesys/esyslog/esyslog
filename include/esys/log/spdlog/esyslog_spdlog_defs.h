/*!
 * \file esys/log/spdlog/esyslog_spdlog_defs.h
 * \brief Definitions needed for esyslog_spdlog
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef ESYSLOG_SPDLOG_EXPORTS
#define ESYSLOG_SPDLOG_API __declspec(dllexport)
#elif ESYSLOG_SPDLOG_USE
#define ESYSLOG_SPDLOG_API __declspec(dllimport)
#else
#define ESYSLOG_SPDLOG_API
#endif
