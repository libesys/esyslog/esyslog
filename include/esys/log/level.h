/*!
 * \file esys/log/level.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef ERROR
#undef ERROR
#endif

//<swig_inc/>

namespace esys::log
{

enum Level
{
    CRITICAL,
    ERROR,
    WARN,
    INFO,
    DEBUG,
    TRACE
};

} // namespace esys::log
