/*!
 * \file esys/log/loggertype.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

//<swig_inc/>

namespace esys
{

namespace log
{

enum LoggerType
{
    NOT_SET,
    SPDLOG,
};

} // namespace log

} // namespace esys
