/*!
 * \file esys/log/logger_if.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/log/esyslog_defs.h"
#include "esys/log/level.h"

#include <esys/base/log_if.h>

#include <string>

//<swig_inc/>

//<swig>%shared_ptr(esys::log::Logger_if);</swig>

namespace esys::log
{

/*! \class Logger_if esys/log/logger_if.h "esys/log/logger_if.h"
 * \brief Logging interface
 */
class ESYSLOG_API Logger_if : public base::Log_if
{
public:
    //! Constructor
    Logger_if();

    //! Destructor
    ~Logger_if() override;

    virtual void log(Level level, const std::string &msg) = 0;

    virtual void log(const std::string &msg, log::Level level, int debug_level = 0) = 0;

    //! Lock the console so no logs can make it to the console
    virtual void lock_console() = 0;

    //! Unlock the console so logs can make it again to the console
    virtual void unlock_console() = 0;
};

} // namespace esys::log
