/*!
 * \file esys/log/consolelockguard.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/log/esyslog_defs.h"
#include "esys/log/logger_if.h"
#include "esys/log/user.h"

#include <memory>

//<swig_inc/>

namespace esys
{

namespace log
{

template<typename LOG_IF>
class ConsoleLockGuard
{
public:
    ConsoleLockGuard(LOG_IF &logger_if)
        : m_logger_if(logger_if)
    {
        m_logger_if.lock_console();
    }

    ~ConsoleLockGuard()
    {
        m_logger_if.unlock_console();
    }

protected:
    LOG_IF &m_logger_if;
};

template<>
class ConsoleLockGuard<std::shared_ptr<Logger_if>>
{
public:
    ConsoleLockGuard(std::shared_ptr<Logger_if> logger_if)
        : m_logger_if(logger_if)
    {
        m_logger_if->lock_console();
    }

    ~ConsoleLockGuard()
    {
        m_logger_if->unlock_console();
    }

protected:
    std::shared_ptr<Logger_if> m_logger_if;
};

template<>
class ConsoleLockGuard<User>
{
public:
    ConsoleLockGuard(User *user)
        : m_user(user)
    {
        m_user->lock_console();
    }

    ~ConsoleLockGuard()
    {
        m_user->unlock_console();
    }

protected:
    User *m_user = nullptr;
};

} // namespace log

} // namespace esys
