/*!
 * \file esys/log/loggerbase.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/log/esyslog_defs.h"
#include "esys/log/logger_if.h"
#include "esys/log/loggertype.h"

#include <memory>
#include <string>

//<swig_inc/>

//<swig>%shared_ptr(esys::log::LoggerBase);</swig>

namespace esys
{

namespace log
{

/*! \class LoggerBase esys/log/loggerbase.h "esys/log/loggerbase.h"
 * \brief Class to log to console and a log file
 */
class ESYSLOG_API LoggerBase : public Logger_if
{
public:
    //! Constructor
    LoggerBase(const std::string &name = "");

    //! Destructor
    virtual ~LoggerBase();

    LoggerType get_type() const;

    //! Set the name for the logger
    /*!
     * \param[in] name the name of the logger, which must be unique within a process
     */
    void set_name(const std::string &name);

    //! Get the name for the logger
    /*!
     * \return the name the of the logger
     */
    const std::string &get_name();

    //! Add logging to the console, meaning std::cout
    /*!
     * \param[in] log_level the logging level for the console
     * \return 0 if successful, < 0 otherwise
     */
    virtual int add_console(Level log_level = Level::INFO) = 0;

    //! Add logging to the console, meaning std::cout, providing a custom pattern for each log
    /*!
     * \param[in] pattern the pattern to use for each log (as defined in spdlog)
     * \param[in] log_level the logging level for the console
     * \return 0 if successful, < 0 otherwise
     */
    virtual int add_console(const std::string &pattern, Level log_level = Level::INFO) = 0;

    //! Add a basic file for logging
    /*!
     * \param[in] path the path of the log file
     * \param[in] truncate
     * \param[in] log_level the logging level for the log file
     * \return 0 if successful, < 0 otherwise
     */
    virtual int add_basic_file(const std::string &path, bool truncate = false, Level log_level = Level::DEBUG) = 0;

    //! Add a rotating file for logging
    /*!
     * \param[in] path the path of the log file
     * \param[in] max_file_size_mbytes the max size of the file in MBytes
     * \param[in] max_file_count the maximum number of rotated files
     * \param[in] log_level the logging level for the log file
     * \return 0 if successful, < 0 otherwise
     */
    virtual int add_rotating_file(const std::string &path, std::size_t max_file_size_mbytes, std::size_t max_file_count,
                                  Level log_level = Level::DEBUG) = 0;

    void debug(int level, const std::string &msg) override;
    /*void info(const std::string &msg) override;
    void warn(const std::string &msg) override;
    void error(const std::string &msg) override;
    void critical(const std::string &msg) override; */

    //! Set the log level of the Logger instance
    /*!
     * \param[in] log_level the log level of this Logger instance
     */
    virtual void set_log_level(Level log_level);

    //! Get the log level of the Logger instance
    /*!
     * \return the log level of this Logger instance
     */
    Level get_log_level() const;

    //! Set the flush log level of the Logger instance
    /*!
     * \param[in] log_level the flush log level of this Logger instance
     */
    virtual void set_flush_log_level(Level log_level);

    //! Get the flush log level of the Logger instance
    /*!
     * \return the flush log level of this Logger instance
     */
    log::Level get_flush_log_level() const;

    //! Set the amount of seconds when elapsed, a flush is triggered automatically
    /*!
     * \param[in] flush_every_secs the amount of seconds
     */
    virtual void set_flush_every(int flush_every_secs);

    //! Get the amount of seconds when elapsed, a flush is triggered automatically
    /*!
     * \return the amount of seconds
     */
    int get_flush_every() const;

    //! Set the debug level of the Logger instance
    /*!
     * \param[in] debug_level the debug level of this Logger instance
     */
    virtual void set_debug_level(int debug_level);

    //! Get the debug level of the Logger instance
    /*!
     * \return the debug level of this Logger instance
     */
    int get_debug_level() const;

protected:
    //!< \cond DOXY_IMPL
    virtual void impl_debug(const std::string &msg) = 0;

    void set_type(LoggerType type);

    LoggerType m_type = LoggerType::NOT_SET;     //!< Type of logger
    std::string m_name;                          //!< The name of the Logger
    Level m_log_level = log::Level::ERROR;       //!< The default log level of the Logger
    Level m_flush_log_level = log::Level::ERROR; //!< The default flush log level of the Logger
    int m_debug_level = -1;                      //!< The default debug level
    int m_flush_every_secs = -1;                 //!< Flush automatically after x seconds, -1 means disabled by default
    //!< \endcond
};

} // namespace log

} // namespace esys
