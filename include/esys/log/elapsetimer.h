/*!
 * \file esys/log/elapsetimer.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/log/esyslog_defs.h"
#include "esys/log/logger_if.h"

#include <string>
#include <chrono>
#include <memory>

//<swig_inc/>

namespace esys
{

namespace log
{

class ESYSLOG_API ElapseTimer
{
public:
    ElapseTimer(const std::string &done_msg, std::shared_ptr<Logger_if> logger_if, Level log_level,
                int debug_level = 0);
    ElapseTimer(const std::string &start_msg, const std::string &done_msg, std::shared_ptr<Logger_if> logger_if,
                Level log_level, int debug_level = 0);
    ~ElapseTimer();

protected:
    std::string m_done_msg;
    std::shared_ptr<Logger_if> m_logger_if;
    Level m_log_level;
    int m_debug_level = 0;
    std::chrono::time_point<std::chrono::steady_clock> m_start_time;
};

} // namespace log

} // namespace esys
