/*!
 * \file esys/log/esyslog_defs.h
 * \brief Definitions needed for esyslog
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef ESYSLOG_EXPORTS
#define ESYSLOG_API __declspec(dllexport)
#elif ESYSLOG_USE
#define ESYSLOG_API __declspec(dllimport)
#else
#define ESYSLOG_API
#define ESYS_STATIC_LINK 1
#endif

#include "esys/log/autolink.h"
