/*!
 * \file esys/log/mngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/log/esyslog_defs.h"
#include "esys/log/loggerbase.h"

#include <memory>
#include <string>
#include <vector>

//<swig_inc/>

namespace esys
{

namespace log
{

class ESYSLOG_API MngrImpl;

/*! \class Mngr esys/log/mngr.h "esys/log/mngr.h"
 * \brief Mngr
 */
class ESYSLOG_API Mngr
{
public:
    //! Destructor
    virtual ~Mngr();

    std::shared_ptr<LoggerBase> new_default_logger(const std::string &name = "");
    std::shared_ptr<LoggerBase> new_logger(LoggerType type, const std::string &name = "");

    void set_search_folder(const std::string &search_folder);
    const std::string &get_search_folder() const;

    int find_plugin_folders(std::vector<std::string> &plugin_folders);

    MngrImpl *get_impl();             //<swig_out/>
    const MngrImpl *get_impl() const; //<swig_out/>

    static std::shared_ptr<Mngr> get();

protected:
    struct MakeSharedEnabler;

    //! Constructor
    Mngr();

    int load_if_not_loaded();

    static std::shared_ptr<Mngr> s_mngr;

    std::unique_ptr<MngrImpl> m_impl;
};

} // namespace log

} // namespace esys
