/*!
 * \file esys/log/trace.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "esys/log/esyslog_defs.h"
#include "esys/log/loggerbase.h"

#include <esys/trace/log_if.h>

#include <memory>

namespace esys::log
{

/*! \class Trace esys/log/trace.h "esys/log/trace.h"
 * \brief
 */
class ESYSLOG_API Trace : public trace::Log_if
{
public:
    //! Constructor
    Trace();

    //! Destructor
    ~Trace() override;

    void lock() override;
    void unlock() override;
    void write(const std::string &trace) override;

    void set_logger(std::shared_ptr<LoggerBase> logger);
    std::shared_ptr<LoggerBase> get_logger() const;

    void set_debug_level(int debug_level);
    int get_debug_level() const;

private:
    //!< \cond DOXY_IMPL
    std::shared_ptr<LoggerBase> m_logger;
    int m_debug_level = 1;
    //!< \endcond
};

} // namespace esys::log
