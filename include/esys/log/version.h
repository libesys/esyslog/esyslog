/*!
 * \file esys/log/version.h
 * \brief Version info for esyslog
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2020-2023 Michel Gillet
 * Distributed under the MIT License.
 * (See accompanying file LICENSE.txt or
 * copy at https://opensource.org/licenses/MIT)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

// Bump-up with each new version
#define ESYSLOG_MAJOR_VERSION 0
#define ESYSLOG_MINOR_VERSION 0
#define ESYSLOG_RELEASE_NUMBER 1
#define ESYSLOG_VERSION_STRING "ESysLog 0.0.1"

// Must be updated manually as well each time the version above changes
#define ESYSLOG_VERSION_NUM_DOT_STRING "0.0.1"
#define ESYSLOG_VERSION_NUM_STRING "0001"

// nothing should be updated below this line when updating the version

#define ESYSLOG_VERSION_NUMBER (ESYSLOG_MAJOR_VERSION * 1000) + (ESYSLOG_MINOR_VERSION * 100) + ESYSLOG_RELEASE_NUMBER
#define ESYSLOG_BETA_NUMBER 1
#define ESYSLOG_VERSION_FLOAT                                                                 \
    ESYSLOG_MAJOR_VERSION + (ESYSLOG_MINOR_VERSION / 10.0) + (ESYSLOG_RELEASE_NUMBER / 100.0) \
        + (ESYSLOG_BETA_NUMBER / 10000.0)

// check if the current version is at least major.minor.release
#define ESYSLOG_CHECK_VERSION(major, minor, release)                                                          \
    (ESYSLOG_MAJOR_VERSION > (major) || (ESYSLOG_MAJOR_VERSION == (major) && ESYSLOG_MINOR_VERSION > (minor)) \
     || (ESYSLOG_MAJOR_VERSION == (major) && ESYSLOG_MINOR_VERSION == (minor) && ESYSLOG_RELEASE_NUMBER >= (release)))
